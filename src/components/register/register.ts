import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

import { AngularFireAuth } from 'angularfire2/auth';

import { MainPage } from '../../pages/main/main';

export interface UName { email: string; username: string; }

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})

export class RegisterPage {

  private usersCollection: AngularFirestoreCollection<UName>;

  //Getting user's e-mail and password
  @ViewChild('email') email;
  @ViewChild('password') password;
  @ViewChild('password2') password2;
  @ViewChild('name') name;


  constructor(
    private db: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    public fire: AngularFireAuth,
    private alertCtrl: AlertController
  ) {
    //Connection to Firestore' collection "users"
    this.usersCollection = db.collection<UName>('users');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  //Alert box function
  alert(message: string) {
    this.alertCtrl.create({
      title: 'Info',
      subTitle: message,
      buttons: ['OK']
    }).present();
  }

  //Registers users on Firebase
  register() {
    if (this.password.value === this.password2.value) {
      this.fire.auth.createUserWithEmailAndPassword(this.email.value, this.password.value).catch(e => {
        console.log(e)
      })
        .then(
          data => {
            if (this.name.value != '') {
              this.usersCollection.doc(this.email.value).set({
                email: this.email.value,
                username: this.name.value
              });
              this.alert('User registered successfully!');
              this.navCtrl.setRoot(MainPage);
            }
          }
        );
    } else {
      this.alert('Password does not match the confirm password.');
      this.password.value = '';
      this.password2.value = '';
    }
  }

}
