import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';

export interface Item { thought: string; name: string; email: string; id: string; date: number; }

/**
 * Generated class for the ViewYourThoughtsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'view-your-thoughts',
  templateUrl: 'view-your-thoughts.html'
})
export class ViewYourThoughtsComponent {

  email: string;

  private itemsCollection: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;

  thoughtToDelete: AngularFirestoreDocument<Item>;

  constructor(
    private db: AngularFirestore,
    private fire: AngularFireAuth,
    private alertCtrl: AlertController
  ) {
    this.email = fire.auth.currentUser.email;
    console.log('Hello SendComponent Component');
    this.itemsCollection = db.collection<Item>('thoughts', ref => ref.where('email', '==', this.email).orderBy('date', 'desc')); //Connection to Firestore
    this.items = this.itemsCollection.valueChanges(); //Getting data
  }

  //Asks if the user really wishes to delete the thought.
  presentConfirm(item) {
    let alert = this.alertCtrl.create({
      title: 'Delete this thought?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        },
        {
          text: 'Yes',
          handler: () => {
            this.delete(item);
          }
        }
      ]
    });
    alert.present();
  }

  //Deletes the thought in the database
  delete(item: Item) {
    this.thoughtToDelete = this.db.doc<Item>('thoughts/' + item.id);
    this.thoughtToDelete.delete();
  }

}
