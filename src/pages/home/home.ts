import { Component, ViewChild } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

import { RegisterPage } from '../../components/register/register';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { MainPage } from '../main/main';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  //Getting user's e-mail and password
  @ViewChild('email') email;
  @ViewChild('password') password;

  constructor(
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    public fire: AngularFireAuth
  ) {}

  //Alert box function
  alert(message: string) {
    this.alertCtrl.create({
      title: 'Info',
      subTitle: message,
      buttons: ['OK']
    }).present();
  }

  //Logs user in
  login() {
    this.fire.auth.signInWithEmailAndPassword(this.email.value, this.password.value)
      .then(data => {
        this.navCtrl.setRoot(MainPage); //Sends user to the main page
      })
      .catch(error => {
        console.log('Got an error: ', error);
        this.alert(error.message);
        this.password.value = '';
      })
  }

  //Sends user to the register page
  goToRegister() {
    this.navCtrl.push(RegisterPage);
  }

  //Sends user to the forgot password page
  goToForgotPassword() {
    this.navCtrl.push(ForgotPasswordPage);
  }

}
