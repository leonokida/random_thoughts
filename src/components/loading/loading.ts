import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

import { MainPage } from '../../pages/main/main';
import { HomePage } from '../../pages/home/home';

@Component({
  selector: 'page-loading',
  templateUrl: 'loading.html'
})
export class LoadingComponent {

  constructor(
    public navCtrl: NavController,
    public fire: AngularFireAuth,
  ) {}

  ngOnInit() {
    this.fire.auth.onAuthStateChanged(user => {
      if (user) {
        this.navCtrl.setRoot(MainPage);
      }
      else {
        this.navCtrl.setRoot(HomePage);
      }
    })
  }

}