import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YourThoughtsPage } from './your-thoughts';

@NgModule({
  declarations: [
    YourThoughtsPage,
  ],
  imports: [
    IonicPageModule.forChild(YourThoughtsPage),
  ],
})
export class YourThoughtsPageModule {}
