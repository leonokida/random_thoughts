import { Component } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';

export interface Item { thought: string; email: string; id: string; date: number; }

/**
 * Generated class for the ViewComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'view',
  templateUrl: 'view.html'
})
export class ViewComponent {

  private itemsCollection: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;

  constructor(
    private db: AngularFirestore,
  ) {
    this.itemsCollection = db.collection<Item>('thoughts', ref => ref.orderBy('date', 'desc')); //Connection to Firestore
    this.items = this.itemsCollection.valueChanges(); //Getting data
  }

}
