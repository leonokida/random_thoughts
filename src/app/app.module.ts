import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

//Pages
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password'
import { MainPage } from '../pages/main/main';
import { YourThoughtsPage } from '../pages/your-thoughts/your-thoughts';

//Components
import { SendComponent } from '../components/send/send';
import { ViewComponent } from '../components/view/view';
import { ViewYourThoughtsComponent } from '../components/view-your-thoughts/view-your-thoughts';
import { RegisterPage } from '../components/register/register';
import { LoadingComponent } from '../components/loading/loading';

//Firebase imports
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';

//Configuring Firebase
const firebase = {
  apiKey: "AIzaSyAGa4sC8DUJiuBOnvdb6g2zG-axZzWh7io",
  authDomain: "randomthoughts-96a9e.firebaseapp.com",
  databaseURL: "https://randomthoughts-96a9e.firebaseio.com",
  projectId: "randomthoughts-96a9e",
  storageBucket: "randomthoughts-96a9e.appspot.com",
  messagingSenderId: "998657297656"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ForgotPasswordPage,
    RegisterPage,
    MainPage,
    YourThoughtsPage,
    SendComponent,
    ViewComponent,
    ViewYourThoughtsComponent,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebase),
    AngularFireAuthModule,
    AngularFirestoreModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ForgotPasswordPage,
    RegisterPage,
    MainPage,
    YourThoughtsPage,
    SendComponent,
    ViewComponent,
    ViewYourThoughtsComponent,
    LoadingComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
