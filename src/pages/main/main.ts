import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { HomePage } from '../home/home';
import { YourThoughtsPage } from '../your-thoughts/your-thoughts';

/**
 * Generated class for the MainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController,
    public fire: AngularFireAuth,
    public alertCtrl : AlertController
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainPage');
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Do you really wish to quit?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.quit();
          }
        }
      ]
    });
    alert.present();
  }

  presentActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Options',
      buttons: [
        {
          text: 'Your thoughts',
          handler: () => {
            this.yourThoughts();
          }
        },{
          text: 'Quit',
          handler: () => {
            this.presentConfirm();
          }
        },{
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  quit() {
    this.fire.auth.signOut();
    this.navCtrl.setRoot(HomePage);
  }

  yourThoughts() {
    this.navCtrl.push(YourThoughtsPage);
  }

}
