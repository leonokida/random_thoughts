import { Component, ViewChild } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';

export interface Thought { thought: string; name: string; email: string; id: string; date: number; }
export interface UName { email: string; username: string; }

/**
 * Generated class for the SendComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'send',
  templateUrl: 'send.html'
})
export class SendComponent {

  //Getting user's email
  email: string;

  //Used to connect to "thoughts"
  private thoughtsCollection: AngularFirestoreCollection<Thought>;
  thoughts: Observable<Thought[]>;

  //Used to find username
  username: AngularFirestoreDocument<UName>
  name: Observable<UName>;
  usernamefinal: any;

  //Getting user's thought, written in the input
  @ViewChild('thought') thought;

  constructor(
    private db: AngularFirestore,
    private fire: AngularFireAuth
  ) {
    console.log('Hello SendComponent Component');
    this.email = fire.auth.currentUser.email;
    //Connection to Firestore' collection "thoughts"
    this.thoughtsCollection = db.collection<Thought>('thoughts');
    //Connection to "users"
    this.username = db.doc<UName>('users/' + this.email);
    this.name = this.username.valueChanges()
    this.usernamefinal = this.name.subscribe(
      name => {
        this.usernamefinal = name;
      }
    )
  }

  //User's thought and e-mail are added to "thoughts"
  sendThought() {
    if (this.thought.value != '') {

      let id = this.db.createId(); //Thought's ID

      let time = new Date().getTime(); //When the thought was posted

      this.thoughtsCollection.doc(id).set({
        thought: this.thought.value,
        name: this.usernamefinal.username,
        email: this.email,
        id: id,
        date: time
      }).catch(e => {
        console.log(e);
      })
      this.thought.value = '';
    }
  }

}
