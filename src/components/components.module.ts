import { NgModule } from '@angular/core';
import { SendComponent } from './send/send';
import { ViewComponent } from './view/view';
import { ViewYourThoughtsComponent } from './view-your-thoughts/view-your-thoughts';
import { LoadingComponent } from './loading/loading';
import { RegisterPage } from './register/register';
@NgModule({
	declarations: [SendComponent,
    ViewComponent,
    ViewYourThoughtsComponent,
    LoadingComponent,
    RegisterPage],
	imports: [],
	exports: [SendComponent,
    ViewComponent,
    ViewYourThoughtsComponent,
    LoadingComponent,
    RegisterPage]
})
export class ComponentsModule {}
