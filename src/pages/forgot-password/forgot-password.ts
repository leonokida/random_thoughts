import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { HomePage } from '../home/home';
/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  //Getting user's e-mail
  @ViewChild('email') email;

  constructor(
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    public navParams: NavParams,
    public fire: AngularFireAuth
  ) {
  }

  //Alert box function
  alert(message: string) {
    this.alertCtrl.create({
      title: 'Info',
      subTitle: message,
      buttons: ['OK']
    }).present();
  }

  //Sends a reset password e-mail to the user
  resetPassword() {
    if ( this.email.value != '') {
      this.fire.auth.sendPasswordResetEmail(this.email.value).then((res) => {
        this.alert('An e-mail to reset your password was sent to you. Please check your e-mail box.')
        this.navCtrl.push(HomePage);
      })
      .catch((error) => {
        this.alert(error);
      })
    } else {
      this.alert('Please insert your e-mail!')
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }

}
